<?php
/**
 * Functions
 * @package WordPress
 * @subpackage sorteo
 * @since 1.0
 * @version 1.0
 */
/**
 * Si no esta ABSPATH, es debido a intentar
 * acceder a algunos datos sin permiso
 */
if (!defined('ABSPATH')) {
	die('Lo siento amigo, en cuba no nos gusta los intusos');
	exit();
}


add_filter( 'upload_mimes', 'custom_upload_mimes' );
function custom_upload_mimes( $existing_mimes = array() ) {
	// Add the file extension to the array
	$existing_mimes['svg'] = 'image/svg+xml';
	return $existing_mimes;
}

/**
 * Registrar los estilos
 */
function tc_register_styles() {

	/* bootstrapcss */
	wp_register_style(
			'bootstrapcss', get_theme_file_uri('assets/css/bootstrap-v4/bootstrap.min.css'), false, false, 'all'
	);

	/* animate.css */
	wp_register_style(
			'animate', get_theme_file_uri('assets/css/animate.css'), false, false, 'all'
	);
	/* animate.css */
	wp_register_style(
			'responsiveslides', get_theme_file_uri('assets/css/responsiveslides.css'), false, false, 'all'
	);

	/* Font Awesome */
        wp_register_style(
                        'fontawesome', get_theme_file_uri('assets/css/font-wesome-4.6.3/font-awesome.css'), false, false, 'all'
        );

	/* style.css */
	wp_register_style(
			'style', get_stylesheet_uri(), false, false, 'all'
	);
}

/**
 * Agregar los estilos a la plantilla
 */
add_action('wp_enqueue_scripts', 'tc_enqueue_styles');

function tc_enqueue_styles() {

	/* Registrar los estilos */
	tc_register_styles();

	wp_enqueue_style('bootstrapcss');
	wp_enqueue_style('animate');
		wp_enqueue_style('responsiveslides');
  wp_enqueue_style('fontawesome');

	wp_enqueue_style('style');
}

/**
 * Registrar los Scripts
 */
function tc_register_scripts() {

	/* jQuery */
	wp_register_script(
			'jquery', get_theme_file_uri('assets/js/jquery-1.12.0.min.js'), false, false, false
	);

	/* bootstrapjs */
	wp_register_script(
			'bootstrapjs', get_theme_file_uri('assets/js/bootstrap-v4/bootstrap.min.js'), ['jquery'], false, true
	);

	/* mainjs */
	wp_register_script(
			'validate', get_theme_file_uri('assets/js/jquery.validate.js'), ['jquery'], false, true
	);
	/* mainjs */
	wp_register_script(
			'countdown', get_theme_file_uri('assets/js/countdown.js'), ['jquery'], false, true
	);
	/* swiper */
	wp_register_script(
			'responsiveslides', get_theme_file_uri('assets/js/responsiveslides.js'), ['jquery'], false, true
	);

	/* swiper */
	wp_register_script(
			'jquerywaypoints', get_theme_file_uri('assets/js/jquery.waypoints.min.js'), ['jquery'], false, true
	);

	/* mainjs */
	wp_register_script(
			'mainjs', get_theme_file_uri('assets/js/main.js'), ['jquery'], false, true
	);
}

/**
 * Añadir los Scripts a la plantilla
 */
add_action('wp_enqueue_scripts', 'tc_enqueue_scripts');

function tc_enqueue_scripts() {

	/* Eliminar la version de jQuery de Wordpress */
	wp_deregister_script('jquery');

	/* Registrar los Scripts */
	tc_register_scripts();



	wp_enqueue_script('jquery');
	wp_enqueue_script('bootstrapjs');
	wp_enqueue_script('validate');
	wp_enqueue_script('countdown');
	wp_enqueue_script('responsiveslides');
	wp_enqueue_script('jquerywaypoints');
	wp_enqueue_script('mainjs');
	wp_localize_script( 'mainjs', 'admin_ajax', admin_url( 'admin-ajax.php' ) );
}


if(!function_exists('ofd_doctype_opengraph')) {
	function ofd_doctype_opengraph($output) {
		$fb_validation = '
		xmlns:og="http://opengraphprotocol.org/schema/"
		xmlns:fb="http://www.facebook.com/2008/fbml"';
		return $output .  $fb_validation;
	}
}
add_filter('language_attributes', 'ofd_doctype_opengraph');

/*** pasos del sorteo ***/
add_action( 'init', 'cg_sorteo');
/*** pasos del sorteo ***/
add_action( 'init', 'cg_sorteo');
function cg_sorteo(){

		if(isset($_GET['paso']) && ($_GET['paso']!='')){
				$paso_d = explode(':', urldecode($_GET['paso']));
				$paso = $paso_d[0];
 				if($paso==1 && isset($_POST['facebook_id'])){
						// PASOS PARA CONECTAR A LA BASE DE DATOS
						if(is_null(validarIdentificacion($_POST['iden']))){
							$db = conexion();

							if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
								$email = $_POST['email'];
							}
							else{
								$email = '';
							}

							$datos = array(
								'first_name' => $_POST['nombre'],
								'telefono' => $_POST['code']."".$_POST['numero'],
								'email'			=> $email,
								'status' 	=> 'Pendiente',
								'facebook_token' => $_POST['iden']
							);
							/// actualizamos y redireccionamos al siguiente paso
							$actualizar = $db->update( OFD_SUSCRIBE_DB_TABLE, $datos, array( 'facebook_id' => $_POST['facebook_id'] ), array( '%s' ) );
							wp_redirect(home_url("?paso=2&email=".$email.""));
							exit();
						}
						else{
							wp_redirect(home_url());
							exit();
						}
				}

				if($paso==4 && isset($paso_d[1]) && isset($paso_d[2])){

						if(is_null(validarEmail($paso_d[1]))){
							wp_redirect(home_url());
							exit();
						}
							// PASOS PARA CONECTAR A LA BASE DE DATOS
							$db = conexion();
							// generamos el ticket
							$ticket = generarCodigo(6);

							$datos = array(
								'ticket' => $ticket,
								'status'	=> 'Suscrito',
								'fecha_reg' => date('Y-m-d H:i:g')
							);
							// actualizamos los datos del cliente y redirecionamos
							$actualizar = $db->update( OFD_SUSCRIBE_DB_TABLE, $datos, array( 'email' => $paso_d[1] ), array( '%s' ) );

							enviar_email($paso_d[1], $ticket);

							wp_redirect(home_url("?paso=4&ticket=".$ticket.""));
							exit();

						}

						if($paso==4 && isset($_GET['ticket'])){
							if(is_null( validarTicket($_GET['ticket']))){
								wp_redirect(home_url());
								exit();
							}
						}

				}
}

function generarCodigo($longitud) {
	 $key = ''; $pattern = '1234abcdefghijkl5670mnopqrstu89vwxyz';
	 $max = strlen($pattern)-1;
	 for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
	 return strtoupper($key);
}


function validarEmail($email = null) {
	$db = conexion();
	return $db->get_var('SELECT email FROM '.OFD_SUSCRIBE_DB_TABLE.' WHERE email = "' . $email . '";');
}

function validarIdentificacion($iden = null) {
	$db = conexion();
	return $db->get_var('SELECT email FROM '.OFD_SUSCRIBE_DB_TABLE.' WHERE facebook_token = "' . $iden . '";');
}

function validarTicket($ticket = null) {
	$db = conexion();
	return $db->get_var('SELECT ticket FROM '.OFD_SUSCRIBE_DB_TABLE.' WHERE ticket = "' . $ticket . '";');
}

function conexion(){
	 static $db = null;
	 if (is_null($db)) {
		 global $wpdb;
		 $db = $wpdb;
	 }

	 if (!defined('OFD_SUSCRIBE_DB_TABLE')) {
		 define('OFD_SUSCRIBE_DB_TABLE', $db->prefix . 'ofd_suscribe');
	 }

	 return $db;
 }


 /*
   ==================================================================
   Accion para enviar email de consulta
   ==================================================================
  */

 function send_smtp_email( $phpmailer ) {

	 //accedemos a las opciones
	 $opciones = ofd_suscribe_settings();

 // La dirección del HOST del servidor de correo SMTP p.e. smtp.midominio.com
 	$phpmailer->Host = $opciones['ofd_text_field_e0'];

 // Puerto SMTP - Suele ser el 25, 465 o 587
 	$phpmailer->Port = $opciones['ofd_text_field_e1'];

 // Usuario de la cuenta de correo
 	# info@envioscaracol.com
 	$phpmailer->Username = $opciones['ofd_text_field_e2'];

 // Contraseña para la autenticación SMTP
 	# Demo4231#@64
 	$phpmailer->Password = $opciones['ofd_text_field_e3'];

 // Uso autenticación por SMTP (true|false)
 	$phpmailer->SMTPAuth = true;

 // El tipo de encriptación que usamos al conectar - ssl (deprecated) o tls
 #$phpmailer->SMTPSecure = "tls";
 	# "info@onefocusdigital.com"
 	$phpmailer->From = $opciones['ofd_text_field_e4'];
 	#"OnefocusDigital -  Agencia de Publicidad"
 	$phpmailer->FromName = $opciones['ofd_text_field_e5'];

 // Define que estamos enviando por SMTP
 	$phpmailer->isSMTP();
 }


 /*
   ==================================================================
   funcion que envia el email de consulta
   ==================================================================
  */

 function enviar_email($email,$ticket) {

 			$headers = array( 'Content-Type: text/html; charset=UTF-8' );
 			$correo = 'carlosguilarte3@gmail.com';

 			add_action( 'phpmailer_init', 'send_smtp_email' );

			$mensaje = '<b>¡Felicidades!</b>
 				<p>Se ha registrado exitosamente, este es tu número de ticket en el sorteo:</p>
 				<br>
 				<span style="display:block;font-size:24px;font-weight:bold;">'.$ticket.'</span>
 				<br>
				<p>La selección del número ganador se realizará en una de nuestras agencias Portal al Caribe, específicamente en la agencia sita en 4311 Palm Ave. Hialeah, FL 33012 el día 14 de Febrero de 2017.</p>
				<p>Ese mismo día será anunciado por los medios, y el ganador será notificado por e-mail y por vía telefónica.</p>
				<p>Puede obtener cualquier información adicional llamando al teléfono (786) 220 8559, o directamente en nuestras oficinas:</p>
					<ul>
						<li>Flagler: 6726 West Flagler St. Miami. FL. 33144</li>
						<li>Palm Ave: 4311 Palm Ave. Hialeah, FL 33012</li>
						<li>Doral: 7211 NW 46 th St Miami. FL 33166</li>
					</ul>';



 			$result = wp_mail( $email, 'Ticket de Sorteo', $mensaje, $headers, '' );

 			if ( !$result ) {
 				$ts_mail_errors = -1;
 			} else {
 				$ts_mail_errors = 1;
 				//wp_mail( $correo, 'Ticket de Sorteo', $mensaje, $headers, '' );
 			}

 }
