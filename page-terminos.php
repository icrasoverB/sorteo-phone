<?php
/**
 * Page
 *
 * Template Name: Términos y Condiciones
 * @package Wordpress
 * @subpackage ofd-sorteo
 */

 get_header();
?>

<div class="header-terminos">
  <a class="enlace-home" href="<?php echo home_url();?>">
    <div class="regresar-terminos">
    </div>
  </a>
</div>


<?php
if ( have_posts() ) :
	the_post();
?>

	<div class="contenedor-terminos">

		<div class="contenido-terminos">
			<?php the_content(); ?>
		</div>

	</div>


<?php
endif;
get_footer();

?>
