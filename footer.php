<div class="footer-sorteo">

    <div class="pie">


      <div class="contacto">
          <div class="titulo"></div>
            <div class="bloques">

              <div class="rslides_container">
                <ul class="rslides" id="contactos">

                  <div class="items info-alas">
                    <div class="info-contacto">
                      <div class="titulos">
                        6726 West Flagler Street. Miami. Florida. Zip Code 33144 United States Of America
                      </div>

                      <div class="lista">
                          <span>Email : info@portalalcaribe.com</span>
                          <span class="border-white"><a class="tel" href="tel:+7862208559">Phone : + 001 786 220 8559</a></span>
                          <span class="fax">Fax : + 001 800 843 5060</span>
                      </div>

                    </div>
                  </div>

                  <div class="items info-centro">
                    <div class="info-contacto-centro">
                      <div class="titulos">
                        7211 North West 46 Street Miami Florida. Zip Code 33166 United States Of America
                      </div>

                      <div class="lista">
                          <span>Email : info@portalalcaribe.com</span>
                          <span class="border-white"><a class="tel" href="tel:+7862208559">Phone : + 001 786 220 8559</a></span>
                          <span class="fax">Fax : + 001 800 843 5060</span>
                      </div>

                    </div>
                  </div>

                  <div class="items info-alas">
                    <div class="info-contacto">
                      <div class="titulos">
                        4311 Palm Ave. Hialeah, Florida. Zip Code 33012 United States Of America
                      </div>

                      <div class="lista">
                          <span>Email : info@portalalcaribe.com</span>
                          <span class="border-white"><a class="tel" href="tel:+7862208559">Phone : + 001 786 220 8559</a></span>
                          <span class="fax">Fax : + 001 800 843 5060</span>
                      </div>

                    </div>
                  </div>

                </ul>
              </div>



            </div>
        </div>

      </div>
    </div><!--fin pie-->

</div>

<div class="footer-pie">
    <div class="info-pie">
        <div class="logo">
        </div>
        <div class="terminos">
            <a href="terminos">Términos & Condiciones</a> ©2017 Portal al Caribe
        </div>
    </div>
</div>

</div>




<!-- Modal -->
<div class="modal fade" id="Mas" tabindex="-1" role="dialog"  aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content content-info">
<div class="modal-body bg-modal">
<div class="cerrar close" data-dismiss="modal" aria-label="Close">

</div>
<div class="contenedor-modal">
<h3>Que incluye:</h3>
<p>Estadía en el hotel Meliá Marina Varadero.
Boleto Aéreo: Salida desde - Miami - Varadero - Miami
Voucher de valor $30 dólares para transfer in – out
Impuestos.<br>
Valor del paquete $ 1,050 USD por 2 personas.</p>

<h5>Que no incluye:</h5>

<p>Gastos de hotel individualizados (Mini bar, servicio
de habitaciones, lavandería, etc.)
Propinas para guía y chofer.
Seguro de cancelación de viaje.
Wi-Fi / acceso a Internet excepto cuando se suministra
en el programa.<br>
Tarifa de pago por exceso de equipaje.</p>
</div>

</div>
</div>
</div>
</div>



<?php wp_footer(); ?>
</body>
</html>
<?php
    ob_end_flush(); // Flush the output from the buffer
?>
