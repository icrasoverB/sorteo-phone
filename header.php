<?php
    ob_start(); // Initiate the output buffer
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>..::: Sorteo :::..</title>
    <meta name = "format-detection" content = "telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="Hotel Resort Melia Marina Varadero" />
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="<?php echo home_url();?>/"/>
		<meta property="og:site_name" content="onefocusdigital"/>
    <meta property="og:description" content="Celebra el 14 de febrero en estilo, participa y gana un fin de semana para 2 personas en el Hotel Resort Melia Marina Varadero con todo incluido, valorado en $1,050.00" />
    <meta property="og:image" content="<?= bloginfo( 'stylesheet_directory' ); ?>/assets/img/sorteo_facebook.jpg" />
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php do_action('wp_head'); ?>
    <script type="text/javascript">
        var url_4 = "<?php echo home_url('?paso=4');?>";
        var url_3 = "<?php echo home_url('?paso=3');?>";
        var home_url = "<?php echo home_url();?>";
        var appID = "<?php appIdFacebook();?>";
    </script>
  </head>
  <body>
      <div class="device">
