<?php get_header(); ?>

<?php
    $pasos =  isset($_GET['paso']) ? $_GET['paso']:0;

    $nombre = '';
    $email = '';
    $fb_id = '';
    $ticket = '';

    switch ($pasos) {
      case 0:
            $pasos = 0;
            unset($_COOKIE["primeraVez"]);
            break;
      case 1:
            if(isset($_GET['fb_id'])){
              $nombre = $_GET['nombre']." ".$_GET['apellido'];
              $email = $_GET['email'];
              $facebook_id = $_GET['fb_id'];
              $iden = $_GET['iden'];
            }else{
              $pasos = 0;
            }
            break;
      case 2:
            if(isset($_GET['email'])){
              $email = $_GET['email'];
            }
            else{
              $pasos=0;
            }
            break;
      case 3:
            if(isset($_GET['email'])){
              $p3_email = $_GET['email'];
            }
            else{
              $pasos=0;
            }
            break;
      case 4:
           if($pasos==4 && isset($_GET['ticket'])){
              $ticket = $_GET['ticket'];
            }
            else{
              $pasos=0;
            }
            break;
        }

 ?>



        <div class="header">
              <div class="popup popup-mas">

              </div>
        </div>

          <div class="contador">
            <div class="header_tiempo">

                <div class="tiempo">
                  <div class="caja">
                      <small class="titulo days">00</small>
                      <small class="descripcion timeRefDays">DIAS</small>
                  </div>

                </div>
                <div class="tiempo">
                  <div class="caja">
                      <small class="titulo hours">00</small>
                      <small class="descripcion timeRefHours">HORAS</small>
                  </div>
                </div>
                <div class="tiempo">
                  <div class="caja">
                      <small class="titulo minutes">00</small>
                      <small class="descripcion timeRefMinutes">MINUTOS</small>
                  </div>
                </div>
                <div class="tiempo">
                  <div class="caja">
                      <small class="titulo"><span>-</span><span class="seconds"></span></small>
                      <small class="descripcion timeRefSeconds">SEGUNDOS</small>
                  </div>
                </div>

              </div>
          </div>



          <div class="contenido-formulario">
            <div class="contenedor-formulario">

              <div class="paso_1 mt-50" style="display:<?=$pasos==0 ? 'block':'none';?>">

                <div class="f-titulo">
                    Regístrate GRATIS!
                </div>
                <div class="f-subtitulo w-100">
                  Para participar en el sorteo solo tienes que completar
                  3 sencillos pasos, comenzando por registro con Facebook.
                </div>

                <a href="<?php echo ofd_suscribe_url_suscripcion() ?>" class="enlace disabled-link">
                  <div class="bton-facebook facebook-link">Iniciar con Facebook</div>
                </a>

                <div class="f_terminos">
                    <input type="checkbox" checked name="form_terminos" id="form_terminos">
                    <label for="form_terminos"> <a style="color:#fff;" href="<?php echo home_url('terminos');?>">Acepto los términos y condiciones</a></label>
                </div>
              </div>

              <div class="paso_2" style="display:<?=$pasos==1 ? 'block':'none';?>">

                <?php
                if($pasos==1){
                ?>
                <script type="text/javascript">
                $(document).ready(function() {
                  $('html, body').animate({
                      scrollTop: $(".paso_2").offset().top-100
                  }, 2000);
                });
                </script>
                <?php } ?>

                <div class="f-titulo">
                    Verifica tus datos
                </div>
                <div class="f-subtitulo">
                  Es importante que verifiques toda la información en el siguiente formulario antes de continuar.
                </div>

                <div class="formulario">
                  <div class="error_datos">
                      El telefono es obligatorio
                      <div id="flechaAbajo">
                      </div>
                  </div>
                  <div class="error_datos_email">
                      Debe de ingresar un email valido.
                      <div id="flechaAbajo">
                      </div>
                  </div>

                  <form name="formulario_paso_2" id="formulario_paso_2" action="?paso=1" method="post">
                    <div class="campo">
                        <div class="f-name validate-name"></div>
                        <input type="text" name="nombre" id="nombre" value="<?=$nombre;?>" autocomplete="off">
                        <input type="hidden" name="facebook_id" id="facebook_id" value="<?=$facebook_id;?>">
                        <input type="hidden" name="iden" id="iden" value="<?=$iden;?>">
                    </div>
                    <div class="campo email-validate">
                      <div class="f-email"></div>
                      <input type="text" name="email<?=$email!='' ? '_f':'';?>" <?=$email!='' ? 'disabled':'';?> id="email<?=$email!='' ? '_f':'';?>" value="<?=$email;?>" autocomplete="off">
                      <input type="hidden" name="email<?=$email!='' ? '':'_f';?>"  id="email<?=$email!='' ? '':'_f';?>" value="<?=$email;?>">
                    </div>
                    <div class="grupar">
                      <div class="campo code-movil">
                        <div class="f-tel"></div>
                        <input type="text" name="code" id="code" maxlength="3" autocomplete="off" value="+01">
                      </div>
                      <div class="campo number-movil">
                        <div class="f-tel"></div>
                        <input type="text" name="numero" id="numero" maxlength="10" onkeypress="return Numero(event)" autocomplete="off">
                      </div>
                    </div>
                  </form>
                  <div class="campo center">
                      <button class="boton-formulario" id="bton-paso2" type="button" name="button">Continuar</button>
                  </div>
                </div>

              </div>

              <div class="paso_3 mt-25" style="display:<?=$pasos==2 ? 'block':'none';?>">

                <?php
                if($pasos==2){
                ?>
                <script type="text/javascript">
                $(document).ready(function() {
                  $('html, body').animate({
                      scrollTop: $(".paso_3").offset().top-100
                  }, 2000);
                });
                </script>
                <?php } ?>

                    <div class="f-titulo">
                        Regálanos un “Me Gusta”
                    </div>
                    <div class="f-subtitulo w-100">
                      Únete a nuestra página de Facebook para enterarte de futuros sorteos y promociones disponibles.
                    </div>

                    <div class="f-subtitulo w-100 pt-10">
                      <b class="alerta">IMPORTANTE:</b> Para terminar el proceso de registro en el sorteo debe completar todos los pasos incluyendo darle 'ME GUSTA' para poder CONTINUAR
                    </div>

                    <div class="formulario">
                        <input type="hidden" name="p2_email" id="p2_email" value="<?=$email;?>">
                        <div class="cuadro-megusta">
                            <div id="fb-root"></div>
                              <script>(function(d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s); js.id = id;
                                js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8&appId=<?php appIdFacebook(); ?>";
                                fjs.parentNode.insertBefore(js, fjs);
                              }(document, 'script', 'facebook-jssdk'));</script>
                              <div class="fb-like" data-href="https://www.facebook.com/PortalAlCaribe/" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true" data-share="false"></div>
                              <script type="text/javascript">
                                window.fbAsyncInit = function() {


                                    FB.Event.subscribe('edge.create',
                                        function(response) {
                                          var email = $('#p2_email').val();
                                          location.href=url_3+'&email='+email+'';
                                        }
                                    );
                                };
                                </script>
                        </div>

                        <div class="campo center mt-menos-10">
                            <button class="boton-formulario"  id="megusta" type="button" name="button">Continuar</button>
                        </div>
                    </div>

              </div>
              <div class="paso_4 mt-25" style="display:<?=$pasos==3 ? 'block':'none';?>">
                    <?php
                    if($pasos==3){
                    ?>
                    <script type="text/javascript">
                    $(document).ready(function() {
                      $('html, body').animate({
                          scrollTop: $(".paso_4").offset().top-100
                      }, 2000);
                    });
                    </script>
                    <?php } ?>
                    <div class="f-titulo">
                        Ya casi terminas!
                    </div>
                    <input type="hidden" name="p3_email" id="p3_email" value="<?=$p3_email;?>">

                    <div class="f-subtitulo w-80">
                      Ayúdanos a correr la voz dando clic en el botón de 'Compartir' para que tus amigos se enteren.
                    </div>

                      <div class="facebook-link  btn-compartir">Compartir con Facebook </div>
                      <div class="formulario">


                         <div class="campo center mt-menos-10 com" style="display:none;">
                           <button class="boton-formulario"  id="continuarCompartir" type="button" name="button">Continuar </button>
                         </div>

                      </div>

              </div>
              <div class="paso_5 mt-25" style="display:<?=$pasos==4 ? 'block':'none';?>">
                <?php
                if($pasos==4){
                ?>
                <script type="text/javascript">
                $(document).ready(function() {
                  $('html, body').animate({
                      scrollTop: $(".paso_5").offset().top-100
                  }, 2000);
                });
                </script>
                <?php } ?>
                    <div class="f-titulo">
                        Felicidades!
                    </div>
                    <div class="f-subtitulo w-100">
                      Te has registrado exitosamente, este es tu número de ticket en el sorteo, revisa tu email para ver todos los detalles.
                    </div>

                    <div class="ticke">
                        <div class="codigo">
                          <span class="code-ticke">#<?=$ticket;?></span>
                          <span class="info-ticke">- subasta no2017501 -</span>
                        </div>
                    </div>

                    <div class="f-subtitulo w-70">
                      EL ganador será anunciado el 02/14/2017 notificandose vía email ó telefónicamente.
                    </div>

              </div>
            </div>
          </div>

</div>




        <div class="razon_sorteo">

        </div>

        <div class="servicios ">
          <div class="rslides_container">
            <ul class="rslides" id="servicios">
              <li>
                <img src="<?= bloginfo( 'stylesheet_directory' ); ?>/assets/img/s_servicio_1.svg" alt="">
                <div class="telefono-contacto"><a href="tel:+7862208559">+ INFO 786 220 8559</a></div>
              </li>
              <li>
                <img src="<?= bloginfo( 'stylesheet_directory' ); ?>/assets/img/s_servicio_2.svg" alt="">
                <div class="telefono-contacto"><a href="tel:+7862208559">+ INFO 786 220 8559</a></div>
              </li>
              <li>
                <img src="<?= bloginfo( 'stylesheet_directory' ); ?>/assets/img/s_servicio_3.svg" alt="">
                <div class="telefono-contacto"><a href="tel:+7862208559">+ INFO 786 220 8559</a></div>
              </li>
            </ul>
          </div>

        </div>
      </div>




    <?php get_footer();?>
