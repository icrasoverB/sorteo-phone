$(document).ready(function() {
	$('.header').css({'height':''+screen.width+''});
	$('.header-terminos').css({'height':''+screen.width+''});
	$(".header_tiempo").countdown({
	date: "14 February 2017 00:00:00", // add the countdown's end date (i.e. 3 november 2012 12:00:00)
	format: "on" // on (03:07:52) | off (3:7:52) - two_digits set to ON maintains layout consistency
	});



	// Slideshow 3
      $("#servicios").responsiveSlides({
        auto: false,
        pager: false,
        nav: true,
        speed: 500,
        maxwidth: 800,
        namespace: "large-btns"
      });

	// Slideshow 3
      $("#contactos").responsiveSlides({
        auto: false,
        pager: false,
        nav: true,
        speed: 500,
        maxwidth: 800,
        namespace: "large-btns"
      });


			$('.contenedor-terminos').waypoint(function(direction) {
					if(direction!='up'){
					 	$('.regresar-terminos').addClass('regresar-terminos-fixed');
				 	}
					else{
						$('.regresar-terminos').removeClass('regresar-terminos-fixed');
					}
			 });

});


$(document).on('click','#bton-paso2', function(){
		if($('#formulario_paso_2 #nombre').val()==''){
					$('#formulario_paso_2 .validate-name').addClass('border-error');
					return false;
		}
		if($('#formulario_paso_2 #email').val()==''){
				$('#formulario_paso_2 .email-validate').addClass('border-error');
				$('.error_datos_email').fadeIn(400);
				return false;
		}
		if($('#formulario_paso_2 #code').val()==''){
						$('#formulario_paso_2 .code-movil').addClass('border-error');
						return false;
		}
		if($('#formulario_paso_2 #numero').val()==''){
						$('#formulario_paso_2 .number-movil').addClass('border-error');
						$('.error_datos').fadeIn(400);
						return false;
		}
		if($('#formulario_paso_2 #code').val()=='' || $('#formulario_paso_2 #numero').val()==''){
					$('#formulario_paso_2 .code-movil').addClass('border-error');
					$('#formulario_paso_2 .number-movil').addClass('border-error');
					$('.error_datos').fadeIn(400);
					return false;
		}

		document.formulario_paso_2.submit();
});

$(document).on('keydown','#nombre',function(){
		$('#formulario_paso_2 .validate-name').removeClass('border-error');
});
$(document).on('keydown','#code',function(){
		$('#formulario_paso_2 .code-movil').removeClass('border-error');
});
$(document).on('keydown','#numero',function(){
		$('#formulario_paso_2 .number-movil').removeClass('border-error');
		$('.error_datos').fadeOut(400);
});

$(document).on('keydown','#email',function(){
		$('#formulario_paso_2 .email-validate').removeClass('border-error');
		$('.error_datos_email').fadeOut(400);
});


$(document).on('click', "#form_terminos", function() {
			if($("#form_terminos").is(':checked')) {
					$('.enlace').removeAttr('disabled');
					$('.enlace').removeClass('disabled-link');
					$('.enlace .bton-facebook').removeClass('facebook-link-disabled');
					$('.enlace .bton-facebook').addClass('facebook-link');
			} else {
					$('.enlace').attr("disabled", 'disabled');
					$('.enlace').addClass('disabled-link');
					$('.enlace .bton-facebook').removeClass('facebook-link');
					$('.enlace .bton-facebook').addClass('facebook-link-disabled');

			}
	});

$(document).on('click','.popup-mas', function(){
    $('#Mas').modal();
		$('.modal-backdrop').removeClass('in');
});

$(document).on('click','#megusta', function(){
	var email = $('#p2_email').val();
	location.href=url_3+'&email='+email+'';
});

$(document).on('click','#continuarCompartir', function(){
	var email = $('#p3_email').val();
	location.href=url_4+':'+email+':0';
});


$(document).on('click', '.btn-compartir', function(){
	 var email = $('#p3_email').val();
	 var title = 'Hotel Resort Melia Marina Varadero';
	 var des 	 = 'Celebra el 14 de febrero en estilo, participa y gana un fin de semana para 2 personas en el Hotel Resort Melia Marina Varadero con todo incluido, valorado en $1,050.00';
	 var url_shared = 'https://www.facebook.com/dialog/share?app_id='+appID+'&display=popup&href='+home_url+'&title='+title+'&description='+des+'&redirect_uri='+url_4+':'+email+':0';
	 location.href = url_shared;
	 $('.com').show();
	 $('.com').css({'display':'block'});
});

setInterval("verificarConteo()",10000);

function verificarConteo() {
		var dia = $('.caja .days').text();
		var hora = $('.caja .hours').text();
		var minutos = $('.caja .minutes').text();
		var segundos = $('.caja .seconds').text();

		if(dia=='00' && hora=='00' && minutos=='00'){
				$('.f_terminos').hide();
				$('.paso_1 .enlace').attr('href','#');
				$('.bton-facebook').removeClass('facebook-link').addClass('facebook-link-disabled');
		}

}
